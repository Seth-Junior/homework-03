import React from 'react'
import { Button, View, Text, Modal, TouchableHighlight, StyleSheet, ImageBackground, Touchable } from 'react-native'

const ModalComponent = (props) => {

    function onClose() {
        props.onParentClose(false)
    }

    // console.log("data:", props.data);

    return (
        <Modal visible={props.isShow} animationType='slide' >
            <View style={styles.cotainer}>
                <ImageBackground resizeMode="cover" style={styles.imageCSS} source={{ uri: props.data.img }}>
                    <TouchableHighlight onPress={onClose} style={styles.buttonStyle}>
                        <Text style={{ color: 'white', fontSize: 20 }}>Close</Text>
                    </TouchableHighlight>
                    {
                        !props.data.title ? <Text></Text> :
                            <View style={{ alignItems: 'center' }}>
                                <Text style={{ color: 'white', width: '100%', backgroundColor: 'black', padding: 15, margin: 'auto' }}>
                                    {props.data.title}
                                </Text>
                            </View>
                    }
                </ImageBackground>
            </View>
        </Modal>
    )
}

export default ModalComponent


const styles = StyleSheet.create({
    cotainer: {
        flex: 1,
    },
    imageCSS: {
        flex: 1,
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        paddingVertical: 60
    },
    buttonStyle: {
        paddingVertical: 4,
        borderRadius: 10,
        backgroundColor: "red",
        marginLeft: '70%',
        justifyContent: 'center',
        alignItems: 'center'
    },
})
